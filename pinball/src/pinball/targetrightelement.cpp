#include "orx.h"
#include "element.h"
#include "targetrightelement.h"

targetrightelement::targetrightelement(int points) : element(points, (orxCHAR*)"TargetRightObject", orxNULL) {
	activatorHitAnimationName = (orxCHAR*)"TargetRightHitAnim";
	activatorAnimationName = (orxCHAR*)"TargetRightOffAnim";
	lightOnAnimationName = orxNULL;
	//activatorFlashAnimationName = (orxCHAR*)"TargetLightFlashAnim";
}

targetrightelement::~targetrightelement()
{
}

void targetrightelement::RunRules() {
	//orxLOG("----------------targetrightelement RunRules");
	element::RunRules();
}
