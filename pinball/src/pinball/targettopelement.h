#ifndef TARGETTOPELEMENT_H
#define TARGETTOPELEMENT_H

class targettopelement : public element
{
public:
	targettopelement(int points);
	~targettopelement();
	
	void RunRules();
};

#endif // TARGETTOPELEMENT_H
