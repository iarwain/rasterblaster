##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=pinball
ConfigurationName      :=Debug
WorkspacePath          :=C:/Work/Dev/orx-projects/rasterblaster/pinball/build/windows/codelite
ProjectPath            :=C:/Work/Dev/orx-projects/rasterblaster/pinball/build/windows/codelite
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=sausage
Date                   :=03/01/2017
CodeLitePath           :="C:/Program Files (x86)/CodeLite"
LinkerName             :=C:/MinGW-5.3.0/bin/g++.exe
SharedObjectLinkerName :=C:/MinGW-5.3.0/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../../../bin/windows/pinballd.exe
Preprocessors          :=$(PreprocessorSwitch)__orxDEBUG__ 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="pinball.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=C:/MinGW-5.3.0/bin/windres.exe
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)../../../include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)orxd $(LibrarySwitch)winmm 
ArLibs                 :=  "orxd" "winmm" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)../../../lib/windows $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := C:/MinGW-5.3.0/bin/ar.exe rcu
CXX      := C:/MinGW-5.3.0/bin/g++.exe
CC       := C:/MinGW-5.3.0/bin/gcc.exe
CXXFLAGS :=  -g -msse2 -ffast-math -fno-exceptions $(Preprocessors)
CFLAGS   :=  -msse2 -ffast-math -g -fno-exceptions $(Preprocessors)
ASFLAGS  := 
AS       := C:/MinGW-5.3.0/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files (x86)\CodeLite
Objects0=$(IntermediateDirectory)/bumperelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/directionrolloverelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/element.cpp$(ObjectSuffix) $(IntermediateDirectory)/elementgroup.cpp$(ObjectSuffix) $(IntermediateDirectory)/plunger.cpp$(ObjectSuffix) $(IntermediateDirectory)/rolloverlightelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/slingshotelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/targetrightelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/pinballbase.cpp$(ObjectSuffix) $(IntermediateDirectory)/rasterblaster.cpp$(ObjectSuffix) \
	$(IntermediateDirectory)/_pinball.cpp$(ObjectSuffix) $(IntermediateDirectory)/spinnerelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/clawelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/laneelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/targettopelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/shieldelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/indicators.cpp$(ObjectSuffix) $(IntermediateDirectory)/resources.rc$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@$(MakeDirCommand) "$(ConfigurationName)"


$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "$(ConfigurationName)"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/bumperelement.cpp$(ObjectSuffix): ../../../src/pinball/bumperelement.cpp $(IntermediateDirectory)/bumperelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/bumperelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/bumperelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/bumperelement.cpp$(DependSuffix): ../../../src/pinball/bumperelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/bumperelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/bumperelement.cpp$(DependSuffix) -MM ../../../src/pinball/bumperelement.cpp

$(IntermediateDirectory)/bumperelement.cpp$(PreprocessSuffix): ../../../src/pinball/bumperelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/bumperelement.cpp$(PreprocessSuffix)../../../src/pinball/bumperelement.cpp

$(IntermediateDirectory)/directionrolloverelement.cpp$(ObjectSuffix): ../../../src/pinball/directionrolloverelement.cpp $(IntermediateDirectory)/directionrolloverelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/directionrolloverelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/directionrolloverelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/directionrolloverelement.cpp$(DependSuffix): ../../../src/pinball/directionrolloverelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/directionrolloverelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/directionrolloverelement.cpp$(DependSuffix) -MM ../../../src/pinball/directionrolloverelement.cpp

$(IntermediateDirectory)/directionrolloverelement.cpp$(PreprocessSuffix): ../../../src/pinball/directionrolloverelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/directionrolloverelement.cpp$(PreprocessSuffix)../../../src/pinball/directionrolloverelement.cpp

$(IntermediateDirectory)/element.cpp$(ObjectSuffix): ../../../src/pinball/element.cpp $(IntermediateDirectory)/element.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/element.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/element.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/element.cpp$(DependSuffix): ../../../src/pinball/element.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/element.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/element.cpp$(DependSuffix) -MM ../../../src/pinball/element.cpp

$(IntermediateDirectory)/element.cpp$(PreprocessSuffix): ../../../src/pinball/element.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/element.cpp$(PreprocessSuffix)../../../src/pinball/element.cpp

$(IntermediateDirectory)/elementgroup.cpp$(ObjectSuffix): ../../../src/pinball/elementgroup.cpp $(IntermediateDirectory)/elementgroup.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/elementgroup.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/elementgroup.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/elementgroup.cpp$(DependSuffix): ../../../src/pinball/elementgroup.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/elementgroup.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/elementgroup.cpp$(DependSuffix) -MM ../../../src/pinball/elementgroup.cpp

$(IntermediateDirectory)/elementgroup.cpp$(PreprocessSuffix): ../../../src/pinball/elementgroup.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/elementgroup.cpp$(PreprocessSuffix)../../../src/pinball/elementgroup.cpp

$(IntermediateDirectory)/plunger.cpp$(ObjectSuffix): ../../../src/pinball/plunger.cpp $(IntermediateDirectory)/plunger.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/plunger.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/plunger.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/plunger.cpp$(DependSuffix): ../../../src/pinball/plunger.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/plunger.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/plunger.cpp$(DependSuffix) -MM ../../../src/pinball/plunger.cpp

$(IntermediateDirectory)/plunger.cpp$(PreprocessSuffix): ../../../src/pinball/plunger.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/plunger.cpp$(PreprocessSuffix)../../../src/pinball/plunger.cpp

$(IntermediateDirectory)/rolloverlightelement.cpp$(ObjectSuffix): ../../../src/pinball/rolloverlightelement.cpp $(IntermediateDirectory)/rolloverlightelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/rolloverlightelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/rolloverlightelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/rolloverlightelement.cpp$(DependSuffix): ../../../src/pinball/rolloverlightelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/rolloverlightelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/rolloverlightelement.cpp$(DependSuffix) -MM ../../../src/pinball/rolloverlightelement.cpp

$(IntermediateDirectory)/rolloverlightelement.cpp$(PreprocessSuffix): ../../../src/pinball/rolloverlightelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/rolloverlightelement.cpp$(PreprocessSuffix)../../../src/pinball/rolloverlightelement.cpp

$(IntermediateDirectory)/slingshotelement.cpp$(ObjectSuffix): ../../../src/pinball/slingshotelement.cpp $(IntermediateDirectory)/slingshotelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/slingshotelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/slingshotelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/slingshotelement.cpp$(DependSuffix): ../../../src/pinball/slingshotelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/slingshotelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/slingshotelement.cpp$(DependSuffix) -MM ../../../src/pinball/slingshotelement.cpp

$(IntermediateDirectory)/slingshotelement.cpp$(PreprocessSuffix): ../../../src/pinball/slingshotelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/slingshotelement.cpp$(PreprocessSuffix)../../../src/pinball/slingshotelement.cpp

$(IntermediateDirectory)/targetrightelement.cpp$(ObjectSuffix): ../../../src/pinball/targetrightelement.cpp $(IntermediateDirectory)/targetrightelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/targetrightelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/targetrightelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/targetrightelement.cpp$(DependSuffix): ../../../src/pinball/targetrightelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/targetrightelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/targetrightelement.cpp$(DependSuffix) -MM ../../../src/pinball/targetrightelement.cpp

$(IntermediateDirectory)/targetrightelement.cpp$(PreprocessSuffix): ../../../src/pinball/targetrightelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/targetrightelement.cpp$(PreprocessSuffix)../../../src/pinball/targetrightelement.cpp

$(IntermediateDirectory)/pinballbase.cpp$(ObjectSuffix): ../../../src/pinball/pinballbase.cpp $(IntermediateDirectory)/pinballbase.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/pinballbase.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinballbase.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinballbase.cpp$(DependSuffix): ../../../src/pinball/pinballbase.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinballbase.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/pinballbase.cpp$(DependSuffix) -MM ../../../src/pinball/pinballbase.cpp

$(IntermediateDirectory)/pinballbase.cpp$(PreprocessSuffix): ../../../src/pinball/pinballbase.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinballbase.cpp$(PreprocessSuffix)../../../src/pinball/pinballbase.cpp

$(IntermediateDirectory)/rasterblaster.cpp$(ObjectSuffix): ../../../src/pinball/rasterblaster.cpp $(IntermediateDirectory)/rasterblaster.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/rasterblaster.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/rasterblaster.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/rasterblaster.cpp$(DependSuffix): ../../../src/pinball/rasterblaster.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/rasterblaster.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/rasterblaster.cpp$(DependSuffix) -MM ../../../src/pinball/rasterblaster.cpp

$(IntermediateDirectory)/rasterblaster.cpp$(PreprocessSuffix): ../../../src/pinball/rasterblaster.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/rasterblaster.cpp$(PreprocessSuffix)../../../src/pinball/rasterblaster.cpp

$(IntermediateDirectory)/_pinball.cpp$(ObjectSuffix): ../../../src/pinball/_pinball.cpp $(IntermediateDirectory)/_pinball.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/_pinball.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/_pinball.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/_pinball.cpp$(DependSuffix): ../../../src/pinball/_pinball.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/_pinball.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/_pinball.cpp$(DependSuffix) -MM ../../../src/pinball/_pinball.cpp

$(IntermediateDirectory)/_pinball.cpp$(PreprocessSuffix): ../../../src/pinball/_pinball.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/_pinball.cpp$(PreprocessSuffix)../../../src/pinball/_pinball.cpp

$(IntermediateDirectory)/spinnerelement.cpp$(ObjectSuffix): ../../../src/pinball/spinnerelement.cpp $(IntermediateDirectory)/spinnerelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/spinnerelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/spinnerelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/spinnerelement.cpp$(DependSuffix): ../../../src/pinball/spinnerelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/spinnerelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/spinnerelement.cpp$(DependSuffix) -MM ../../../src/pinball/spinnerelement.cpp

$(IntermediateDirectory)/spinnerelement.cpp$(PreprocessSuffix): ../../../src/pinball/spinnerelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/spinnerelement.cpp$(PreprocessSuffix)../../../src/pinball/spinnerelement.cpp

$(IntermediateDirectory)/clawelement.cpp$(ObjectSuffix): ../../../src/pinball/clawelement.cpp $(IntermediateDirectory)/clawelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/clawelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/clawelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/clawelement.cpp$(DependSuffix): ../../../src/pinball/clawelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/clawelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/clawelement.cpp$(DependSuffix) -MM ../../../src/pinball/clawelement.cpp

$(IntermediateDirectory)/clawelement.cpp$(PreprocessSuffix): ../../../src/pinball/clawelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/clawelement.cpp$(PreprocessSuffix)../../../src/pinball/clawelement.cpp

$(IntermediateDirectory)/laneelement.cpp$(ObjectSuffix): ../../../src/pinball/laneelement.cpp $(IntermediateDirectory)/laneelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/laneelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/laneelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/laneelement.cpp$(DependSuffix): ../../../src/pinball/laneelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/laneelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/laneelement.cpp$(DependSuffix) -MM ../../../src/pinball/laneelement.cpp

$(IntermediateDirectory)/laneelement.cpp$(PreprocessSuffix): ../../../src/pinball/laneelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/laneelement.cpp$(PreprocessSuffix)../../../src/pinball/laneelement.cpp

$(IntermediateDirectory)/targettopelement.cpp$(ObjectSuffix): ../../../src/pinball/targettopelement.cpp $(IntermediateDirectory)/targettopelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/targettopelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/targettopelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/targettopelement.cpp$(DependSuffix): ../../../src/pinball/targettopelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/targettopelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/targettopelement.cpp$(DependSuffix) -MM ../../../src/pinball/targettopelement.cpp

$(IntermediateDirectory)/targettopelement.cpp$(PreprocessSuffix): ../../../src/pinball/targettopelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/targettopelement.cpp$(PreprocessSuffix)../../../src/pinball/targettopelement.cpp

$(IntermediateDirectory)/shieldelement.cpp$(ObjectSuffix): ../../../src/pinball/shieldelement.cpp $(IntermediateDirectory)/shieldelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/shieldelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/shieldelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/shieldelement.cpp$(DependSuffix): ../../../src/pinball/shieldelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/shieldelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/shieldelement.cpp$(DependSuffix) -MM ../../../src/pinball/shieldelement.cpp

$(IntermediateDirectory)/shieldelement.cpp$(PreprocessSuffix): ../../../src/pinball/shieldelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/shieldelement.cpp$(PreprocessSuffix)../../../src/pinball/shieldelement.cpp

$(IntermediateDirectory)/indicators.cpp$(ObjectSuffix): ../../../src/pinball/indicators.cpp $(IntermediateDirectory)/indicators.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/indicators.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/indicators.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/indicators.cpp$(DependSuffix): ../../../src/pinball/indicators.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/indicators.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/indicators.cpp$(DependSuffix) -MM ../../../src/pinball/indicators.cpp

$(IntermediateDirectory)/indicators.cpp$(PreprocessSuffix): ../../../src/pinball/indicators.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/indicators.cpp$(PreprocessSuffix)../../../src/pinball/indicators.cpp

$(IntermediateDirectory)/resources.rc$(ObjectSuffix): ../../../src/pinball/resources.rc
	$(RcCompilerName) -i "C:/Work/Dev/orx-projects/rasterblaster/pinball/src/pinball/resources.rc" $(RcCmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/resources.rc$(ObjectSuffix) $(RcIncludePath)

-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/


